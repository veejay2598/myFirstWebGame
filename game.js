var lion = document.getElementById("lion");
var samson = document.getElementById("samson");
var delilah = document.getElementById("delilah");
var pScore = document.getElementById("playerScore");
var cScore = document.getElementById("compScore");
var pCounter = 0;
var cCounter = 0;
var announceP_icon =document.getElementById("playerIcon");
var announceC_icon = document.getElementById("compIcon");
var winnerPlayer_icon = document.getElementById("playerWin");
var winnerComp_icon = document.getElementById("compWin");
var limitButton = document.getElementById("limitButton");
var limit = 0;

limitButton.onclick = function(){
    if(document.getElementById("input").value == ""){
        alert("set first the limit!");
    }
    else{

        limit = parseInt(document.getElementById("input").value);
        alert("limit is set to "+limit);
        main();
        
    }
    
}

function comChoice(){
    var com = ['L','S','D'];
    var random = Math.floor(Math.random()*3);
    return com[random];
}
function main(){
    lion.addEventListener("click" , function(){
        if(pCounter == limit){
            itsOver("PLAYER");
        }
        if(cCounter == limit){
            itsOver("COMPUTER");
        }
        
        if(document.getElementById("input").value == ""){
            alert("oops");
        }
        else{
            onGame("L");
        }
        
    })
    samson.addEventListener("click" , function(){
        if(document.getElementById("input").value == ""){
            alert("oops");
        }
        if(pCounter == limit){
            itsOver("PLAYER");
        }
        if(cCounter == limit){
            itsOver("COMPUTER");
        }
        else{
            onGame("S");
        }
    })
    delilah.addEventListener("click", function(){
        if(document.getElementById("input").value == ""){
            alert("oops");
        }
        if(pCounter == limit){
            itsOver("PLAYER");
        }
        if(cCounter == limit){
            itsOver("COMPUTER");
        }
        else{
            onGame("D");
        }
    })
}
function itsOver(nameWin){
    alert(nameWin + " win the game!");
    location.reload();
}
function wins(){
                document.getElementById("result").innerHTML = "PLAYER WIN!";
                winnerPlayer_icon.src = "winner.png";
                winnerComp_icon.src = "sad.png";
                pCounter++;
                pScore.innerHTML = pCounter;    
}

function loss(){
                document.getElementById("result").innerHTML = "COMPUTER WIN!";
                winnerComp_icon.src = "winner.png";
                winnerPlayer_icon.src = "sad.png";
                cCounter++;
                cScore.innerHTML = cCounter;
}
            

function draw(){
    document.getElementById("result").innerHTML = " IT`S A DRAW!";
    winnerPlayer_icon.src = "happy.png";
    winnerComp_icon.src = "happy.png";
}

function onGame(say){
    var fromCom = comChoice();
    var combine = say + fromCom;
    announce(say , fromCom);
    if(combine == 'SL' || combine == 'LD' || combine == 'DS'){
        wins();
    }
    else if(combine == 'DL' || combine == 'LS' || combine == 'SD'){
        loss();     
    }
    else if(combine == 'DD'|| combine == 'LL'|| combine == 'SS'){
        draw();        
    }
    
}
function announce(player, comp){
    if(player == 'L'){
        announceP_icon.src="lion.png";   
    }
    else if(player == 'S'){
        announceP_icon.src= "samson.png";
    }
    else if(player == 'D'){
        announceP_icon.src = "delilah.png";
    }
    if(comp == 'L'){
        announceC_icon.src="lion.png";
    }
    else if(comp == 'S'){
        announceC_icon.src= "samson.png";
    }
    else if(comp == 'D'){
        announceC_icon.src = "delilah.png";
    }
}

